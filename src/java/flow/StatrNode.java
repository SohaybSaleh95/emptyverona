/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

/**
 *
 * @author Raghad Numan
 */
public class StatrNode extends Node {

    private Node targetNode;
    
    public StatrNode() {
        this.type = "Circle";
        this.name = "Start";
    }

    public Node getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(Node targetNode) {
        this.targetNode = targetNode;
    }

}

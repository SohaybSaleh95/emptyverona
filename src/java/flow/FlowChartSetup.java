/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.primefaces.event.diagram.ConnectEvent;
import org.primefaces.model.diagram.Connection;
import org.primefaces.model.diagram.DefaultDiagramModel;
import org.primefaces.model.diagram.connector.StateMachineConnector;
import org.primefaces.model.diagram.Element;
import org.primefaces.model.diagram.connector.FlowChartConnector;
import org.primefaces.model.diagram.connector.StraightConnector;
import org.primefaces.model.diagram.endpoint.BlankEndPoint;
import org.primefaces.model.diagram.endpoint.DotEndPoint;
import org.primefaces.model.diagram.endpoint.EndPoint;
import org.primefaces.model.diagram.endpoint.EndPointAnchor;
import org.primefaces.model.diagram.endpoint.RectangleEndPoint;
import org.primefaces.model.diagram.overlay.ArrowOverlay;
import org.primefaces.model.diagram.overlay.LabelOverlay;
//import org.primefaces.model.diagram.endpoint.;

/**
 *
 * @author Raghad Numan
 */
@ManagedBean
@SessionScoped
public class FlowChartSetup implements Serializable {

    private List<Node> listOfAllNode = new ArrayList<>();
    int countStart = 1, countEnd = 1, countCond = 1, countAction = 1, countInput = 1, countOutput = 1;
    //statma
    private DefaultDiagramModel model;

    //newNode
    private Node currentNode = new Node();
    private List<Node> listofCurrentNode = new ArrayList<>();
    private Node selectedNode = new Node();
    private String lineHeader;
    private Node targetNode = new Node();

    private EndPoint sourceEndPoint;
    private EndPoint targetEndPoint;

    private ConnectionNodes connectionNode = new ConnectionNodes();
    private List<ConnectionNodes> listOfConnection = new ArrayList<>();

    public void init() {
        selectedNode = new Node();
        connectionNode = new ConnectionNodes();
        listOfConnection = new ArrayList<>();
        countStart = 1;
        countEnd = 1;
        countCond = 1;
        countAction = 1;
        countInput = 1;
        countOutput = 1;
        listOfAllNode = new ArrayList() {
            {
                add(new ActionNode());
                add(new StatrNode());
                add(new EndNode());
                add(new ConditionNode());
                add(new InputObject());
                add(new OutputObject());
            }
        };
        currentNode = new Node();
        listofCurrentNode = new ArrayList<>();
        model = new DefaultDiagramModel();
        model.setMaxConnections(-1);
        model.getDefaultConnectionOverlays().add(new ArrowOverlay(20, 20, 1, 1));
        FlowChartConnector connector = new FlowChartConnector();
        connector.setPaintStyle("{strokeStyle:'#98AFC7', lineWidth:3}");
        connector.setHoverPaintStyle("{strokeStyle:'#5C738B'}");
        model.setDefaultConnector(connector);
        System.out.println("ffff");
    }

    public void deleteNode() {
        model.removeElement(posNode.getElement());
        listofCurrentNode.remove(posNode);
        System.out.println("hhhhhhhhhhhhhhhhhhh : "+posNode.getName());

    }

    public void createNode(String nodeName) {
        Element e = new Element();
        List<EndPoint> listOfEndPointSB = new ArrayList<>();
        List<EndPointAnchor> locPoint = new ArrayList<>();
        currentNode = new Node();
        if (nodeName.equals("Start")) {
            currentNode = new StatrNode();
            if (countStart < 2) {
                e.setData(currentNode);
                locPoint = new ArrayList<>();

                locPoint.add(EndPointAnchor.BOTTOM);
                listOfEndPointSB = createDotEndPoint(locPoint);
                for (EndPoint endPointSB : listOfEndPointSB) {

                    endPointSB.setMaxConnections(1);
                    endPointSB.setTarget(false);
                    e.setDraggable(true);
                    e.setStyleClass("start-node");
                    e.addEndPoint(endPointSB);

                }
                e.setY(1 * countStart + "em");
                countStart++;
                model.addElement(e);

                currentNode.setY(1 * countStart + "em");
                currentNode.setName(nodeName);
                currentNode.setType(new StatrNode().getType());
                currentNode.setElement(e);
                currentNode.setIndex(model.getElements().size() + "");
                listofCurrentNode.add(currentNode);
            }
            System.out.println(" strat ele id : " + e.getId());
        } else if (nodeName.equals("End")) {
            currentNode = new EndNode();
            if (countEnd < 2) {
                e.setData(currentNode);
                locPoint = new ArrayList<>();
                locPoint.add(EndPointAnchor.TOP);
                locPoint.add(EndPointAnchor.LEFT);
                locPoint.add(EndPointAnchor.RIGHT);
                listOfEndPointSB = createDotEndPoint(locPoint);
                for (EndPoint endPointSB : listOfEndPointSB) {
                    endPointSB.setMaxConnections(1);
                    endPointSB.setSource(false);
                    endPointSB.setTarget(true);
                    e.setDraggable(true);
                    e.setStyleClass("start-node");
                    e.addEndPoint(endPointSB);

                }
                e.setX("7em");
                e.setY(1 * countEnd + "em");
                countEnd++;
                model.addElement(e);

                currentNode.setX("7em");
                currentNode.setY(1 * countEnd + "em");
                currentNode.setName(nodeName);
                currentNode.setType(new EndNode().getType());
                currentNode.setElement(e);
                currentNode.setIndex(model.getElements().size() + "");
                listofCurrentNode.add(currentNode);
            }

        } else if (nodeName.equals("Condition")) {
            currentNode = new ConditionNode();
            e.setData(currentNode);
            if (countCond < 2) {
                countCond = 1;
            }
            locPoint = new ArrayList<>();
            locPoint.add(EndPointAnchor.RIGHT);
            locPoint.add(EndPointAnchor.LEFT);
            locPoint.add(EndPointAnchor.TOP);
            listOfEndPointSB = createDotEndPoint(locPoint);
            for (EndPoint endPointSB : listOfEndPointSB) {
                System.out.println(endPointSB.getAnchor());
                if (endPointSB.getAnchor().equals(EndPointAnchor.TOP)) {
                    System.out.println("in");
                    endPointSB.setSource(false);
                    endPointSB.setTarget(true);

                } else {
                    endPointSB.setSource(true);
                    endPointSB.setTarget(false);
                }
                e.setDraggable(true);
                e.setStyleClass("diamond");
                e.addEndPoint(endPointSB);
            }
            e.setX("13em");
            e.setY(1 * countCond + "em");
            countCond += 9;
            model.addElement(e);

            currentNode.setX("13em");
            currentNode.setY(1 * countCond + "em");
            currentNode.setName(nodeName);
            currentNode.setType(new ConditionNode().getType());
            currentNode.setElement(e);
            currentNode.setIndex(model.getElements().size() + "");
            listofCurrentNode.add(currentNode);
            System.out.println(" strat ele id : " + e.getId());
        } else {

            if (nodeName.equals("Action")) {
                currentNode = new ActionNode();
                e.setData(currentNode);
                if (countAction < 2) {
                    countAction = 1;
                }
                e.setX("23em");
                e.setY(1 * countAction + "em");
                countAction += 6;

                currentNode.setX("23em");
                currentNode.setY(1 * countAction + "em");
                currentNode.setName(nodeName);
                currentNode.setType(new ActionNode().getType());
                currentNode.setElement(e);
                currentNode.setIndex(model.getElements().size() + "");
                listofCurrentNode.add(currentNode);
            } else if (nodeName.equals("Input")) {
                currentNode = new InputObject();
                e.setData(currentNode);
                if (countInput < 2) {
                    countInput = 1;
                }
                e.setX("43em");
                e.setY(1 * countInput + "em");
                e.setStyleClass("input-node");
                countInput += 6;

                currentNode.setX("43em");
                currentNode.setY(1 * countInput + "em");
                currentNode.setName(nodeName);
                currentNode.setType(new InputObject().getType());
                currentNode.setElement(e);
                currentNode.setIndex(model.getElements().size() + "");
                listofCurrentNode.add(currentNode);
            } else {
                currentNode = new OutputObject();
                e.setData(currentNode);
                if (countOutput < 2) {
                    countOutput = 1;
                }
                e.setStyleClass("output-node");
                e.setX("63em");
                e.setY(1 * countOutput + "em");
                countOutput += 6;

                currentNode.setX("63em");
                currentNode.setY(1 * countOutput + "em");
                currentNode.setName(nodeName);
                currentNode.setType(new OutputObject().getType());
                currentNode.setElement(e);
                currentNode.setIndex(model.getElements().size() + "");
                listofCurrentNode.add(currentNode);
            }
            locPoint = new ArrayList<>();
            locPoint.add(EndPointAnchor.RIGHT);
            locPoint.add(EndPointAnchor.LEFT);
            locPoint.add(EndPointAnchor.TOP);
            locPoint.add(EndPointAnchor.BOTTOM);
            listOfEndPointSB = createDotEndPoint(locPoint);
            for (EndPoint endPointSB : listOfEndPointSB) {
                endPointSB.setTarget(true);
                e.setDraggable(true);
                e.addEndPoint(endPointSB);
            }

            model.addElement(e);
        }
        System.out.println(listofCurrentNode.size() + " vv");
    }

    public Connection createConnection(EndPoint from, EndPoint to, String label) {
        Connection conn = new Connection(from, to);
        conn.getOverlays().add(new ArrowOverlay(20, 20, 1, 1));

        if (label != null) {
            conn.getOverlays().add(new LabelOverlay(label, "flow-label", 0.5));
        }

        return conn;
    }

    private List<EndPoint> createDotEndPoint(List<EndPointAnchor> anchor) {

        DotEndPoint endPoint = new DotEndPoint();
        List<EndPoint> listEnd = new ArrayList<>();
        for (int i = 0; i < anchor.size(); i++) {
            endPoint = new DotEndPoint(anchor.get(i));
            endPoint.setMaxConnections(2);
            endPoint.setSource(true);
            endPoint.setScope("network");
            endPoint.setStyle("{fillStyle:'#646D7E'}");
            endPoint.setRadius(8);
            endPoint.setHoverStyle("{fillStyle:'#5C738B'}");
            listEnd.add(endPoint);
        }
        return listEnd;
    }

    public void saveConnection() {
        connectionNode = new ConnectionNodes();
        connectionNode.setSorceNode(selectedNode);
        connectionNode.setTargetNode(targetNode);
        connectionNode.setSorceEndPoint(sourceEndPoint);
        connectionNode.setTargetEndPoint(targetEndPoint);
        connectionNode.setLineLable(lineHeader);
        listOfConnection.add(connectionNode);
        model.connect(createConnection(connectionNode.getSorceEndPoint(), connectionNode.getTargetEndPoint(), connectionNode.getLineLable()));
        lineHeader = "";
    }

    public void position(ConnectEvent e) {
        sourceEndPoint = null;
        targetEndPoint = null;
        targetNode = new Node();
        selectedNode = (Node) e.getSourceElement().getData();
        sourceEndPoint = e.getSourceEndPoint();
        targetEndPoint = e.getTargetEndPoint();
        targetNode = (Node) e.getTargetElement().getData();
        if (selectedNode.getName().equals("Start")) {
            ((StatrNode) selectedNode).setTargetNode((StatrNode) selectedNode);
            if (targetNode.getName().equals("Condition")) {
                ((StatrNode) selectedNode).setTargetNode((ConditionNode) targetNode);
            } else if (targetNode.getName().equals("Action")) {
                ((StatrNode) selectedNode).setTargetNode((ActionNode) targetNode);
            } else if (targetNode.getName().equals("End")) {
                ((StatrNode) selectedNode).setTargetNode((EndNode) targetNode);
            } else if (targetNode.getName().equals("Input")) {
                ((StatrNode) selectedNode).setTargetNode((InputObject) targetNode);
            } else if (targetNode.getName().equals("Output")) {
                ((StatrNode) selectedNode).setTargetNode((OutputObject) targetNode);
            }

        } else if (selectedNode.getName().equals("Action")) {
            if (targetNode.getName().equals("Start")) {
                ((ActionNode) selectedNode).setTargetNode((StatrNode) targetNode);
            } else if (targetNode.getName().equals("Condition")) {
                ((ActionNode) selectedNode).setTargetNode((ConditionNode) targetNode);
            } else if (targetNode.getName().equals("End")) {
                ((ActionNode) selectedNode).setTargetNode((EndNode) targetNode);
            } else if (targetNode.getName().equals("Input")) {
                ((ActionNode) selectedNode).setTargetNode((InputObject) targetNode);
            } else if (targetNode.getName().equals("Output")) {
                ((ActionNode) selectedNode).setTargetNode((OutputObject) targetNode);
            }
        }  else if (selectedNode.getName().equals("Condition")) {
            if (targetNode.getName().equals("Start")) {
                ((ConditionNode) selectedNode).setTargetNode((StatrNode) targetNode);
            } else if (targetNode.getName().equals("End")) {
                ((ConditionNode) selectedNode).setTargetNode((EndNode) targetNode);
            } else if (targetNode.getName().equals("Action")) {
                ((ConditionNode) selectedNode).setTargetNode((ActionNode) targetNode);
            } else if (targetNode.getName().equals("Input")) {
                ((ConditionNode) selectedNode).setTargetNode((InputObject) targetNode);
            } else if (targetNode.getName().equals("Output")) {
                ((ConditionNode) selectedNode).setTargetNode((OutputObject) targetNode);
            }
            
        }
        else if (selectedNode.getName().equals("Input")) {
            if (targetNode.getName().equals("Start")) {
                ((InputObject) selectedNode).setTargetNode((StatrNode) targetNode);
            } else if (targetNode.getName().equals("End")) {
                ((InputObject) selectedNode).setTargetNode((EndNode) targetNode);
            } else if (targetNode.getName().equals("Action")) {
                ((InputObject) selectedNode).setTargetNode((ActionNode) targetNode);
            } else if (targetNode.getName().equals("Condition")) {
                ((InputObject) selectedNode).setTargetNode((ConditionNode) targetNode);
            } else if (targetNode.getName().equals("Output")) {
                ((InputObject) selectedNode).setTargetNode((OutputObject) targetNode);
            }
        }
        else if (selectedNode.getName().equals("Output")) {
            if (targetNode.getName().equals("Start")) {
                ((OutputObject) selectedNode).setTargetNode((StatrNode) targetNode);
            } else if (targetNode.getName().equals("End")) {
                ((OutputObject) selectedNode).setTargetNode((EndNode) targetNode);
            } else if (targetNode.getName().equals("Action")) {
                ((OutputObject) selectedNode).setTargetNode((ActionNode) targetNode);
            } else if (targetNode.getName().equals("Condition")) {
                ((OutputObject) selectedNode).setTargetNode((ConditionNode) targetNode);
            } else if (targetNode.getName().equals("Input")) {
                ((OutputObject) selectedNode).setTargetNode((InputObject) targetNode);
            }
        }
    }
Node posNode = new Node();
    public void onClick() {
        String id = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("elementId");
        String x = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("x");
        String y = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("y");
         posNode = new Node();
        for (Element e : model.getElements()) {
            if (e.getId().equals(id.substring("diagramMain:diagram-".length()))) {
                System.out.println("inn");
                e.setX(x);
                e.setY(y);
                posNode = (Node) e.getData();
                posNode.setX(x);
                posNode.setY(y);
            }

        }
        System.out.println("user clicked on " + id);
        System.out.println("Helllo");
        System.out.println(x);
        System.out.println("y : " + y);

    }

    public List<Node> getListOfAllNode() {
        return listOfAllNode;
    }

    public void setListOfAllNode(List<Node> listOfAllNode) {
        this.listOfAllNode = listOfAllNode;
    }

    public DefaultDiagramModel getModel() {
        return model;
    }

    public void setModel(DefaultDiagramModel model) {
        this.model = model;
    }

    public int getCountStart() {
        return countStart;
    }

    public void setCountStart(int countStart) {
        this.countStart = countStart;
    }

    public int getCountEnd() {
        return countEnd;
    }

    public void setCountEnd(int countEnd) {
        this.countEnd = countEnd;
    }

    public int getCountCond() {
        return countCond;
    }

    public void setCountCond(int countCond) {
        this.countCond = countCond;
    }

    public int getCountAction() {
        return countAction;
    }

    public void setCountAction(int countAction) {
        this.countAction = countAction;
    }

    public int getCountInput() {
        return countInput;
    }

    public void setCountInput(int countInput) {
        this.countInput = countInput;
    }

    public int getCountOutput() {
        return countOutput;
    }

    public void setCountOutput(int countOutput) {
        this.countOutput = countOutput;
    }

    public Node getCurrentNode() {
        return currentNode;
    }

    public void setCurrentNode(Node currentNode) {
        this.currentNode = currentNode;
    }

    public List<Node> getListofCurrentNode() {
        return listofCurrentNode;
    }

    public void setListofCurrentNode(List<Node> listofCurrentNode) {
        this.listofCurrentNode = listofCurrentNode;
    }

    public Node getSelectedNode() {
        return selectedNode;
    }

    public void setSelectedNode(Node selectedNode) {
        this.selectedNode = selectedNode;
    }

    public String getLineHeader() {
        return lineHeader;
    }

    public void setLineHeader(String lineHeader) {
        this.lineHeader = lineHeader;
    }

    public Node getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(Node targetNode) {
        this.targetNode = targetNode;
    }

    public EndPoint getSourceEndPoint() {
        return sourceEndPoint;
    }

    public void setSourceEndPoint(EndPoint sourceEndPoint) {
        this.sourceEndPoint = sourceEndPoint;
    }

    public EndPoint getTargetEndPoint() {
        return targetEndPoint;
    }

    public void setTargetEndPoint(EndPoint targetEndPoint) {
        this.targetEndPoint = targetEndPoint;
    }

    public ConnectionNodes getConnectionNode() {
        return connectionNode;
    }

    public void setConnectionNode(ConnectionNodes connectionNode) {
        this.connectionNode = connectionNode;
    }

    public List<ConnectionNodes> getListOfConnection() {
        return listOfConnection;
    }

    public void setListOfConnection(List<ConnectionNodes> listOfConnection) {
        this.listOfConnection = listOfConnection;
    }

}

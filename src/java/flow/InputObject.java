/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

/**
 *
 * @author Raghad Numan
 */
public class InputObject extends Node {
private Node sourceNode=new Node();

private Node targetNode=new Node();
    public InputObject() {
        this.type = "Input_Comment";
        this.name = "Input";
    }

    public Node getSourceNode() {
        return sourceNode;
    }

    public void setSourceNode(Node sourceNode) {
        this.sourceNode = sourceNode;
    }

    public Node getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(Node targetNode) {
        this.targetNode = targetNode;
    }

}

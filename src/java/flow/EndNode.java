
package flow;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Raghad Numan
 */
public class EndNode extends Node{
    
 private Node sorceNode=new Node();
    public EndNode() {
        this.type = "Circle";
        this.name = "End";
    }

    public Node getSorceNode() {
        return sorceNode;
    }

    public void setSorceNode(Node sorceNode) {
        this.sorceNode = sorceNode;
    }

   
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

import org.primefaces.model.diagram.endpoint.EndPoint;

/**
 *
 * @author Raghad Numan
 */
public class ConnectionNodes extends Node{
    private Node sorceNode;
    private Node targetNode;
    private EndPoint sorceEndPoint;
    private EndPoint targetEndPoint;
    private String lineLable;

    public ConnectionNodes() {
        this.name="Line";
        this.type="Line";
    }

    
    
    public Node getSorceNode() {
        return sorceNode;
    }

    public void setSorceNode(Node sorceNode) {
        this.sorceNode = sorceNode;
    }

    public Node getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(Node targetNode) {
        this.targetNode = targetNode;
    }

    public String getLineLable() {
        return lineLable;
    }

    public void setLineLable(String lineLable) {
        this.lineLable = lineLable;
    }

    public EndPoint getSorceEndPoint() {
        return sorceEndPoint;
    }

    public void setSorceEndPoint(EndPoint sorceEndPoint) {
        this.sorceEndPoint = sorceEndPoint;
    }

    public EndPoint getTargetEndPoint() {
        return targetEndPoint;
    }

    public void setTargetEndPoint(EndPoint targetEndPoint) {
        this.targetEndPoint = targetEndPoint;
    }
    
    
}

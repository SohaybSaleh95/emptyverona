/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package flow;

/**
 *
 * @author Raghad Numan
 */
public class ConditionNode extends Node {

    private Node sorceNode;
    private Node targetNode;
    private Node targetYesNode;
    private Node targetNoNode;
    
    public ConditionNode() {
        this.type = "diamond";
        this.name = "Condition";
    }

    public Node getSorceNode() {
        return sorceNode;
    }

    public void setSorceNode(Node sorceNode) {
        this.sorceNode = sorceNode;
    }

    public Node getTargetYesNode() {
        return targetYesNode;
    }

    public void setTargetYesNode(Node targetYesNode) {
        this.targetYesNode = targetYesNode;
    }

    public Node getTargetNoNode() {
        return targetNoNode;
    }

    public void setTargetNoNode(Node targetNoNode) {
        this.targetNoNode = targetNoNode;
    }

    public Node getTargetNode() {
        return targetNode;
    }

    public void setTargetNode(Node targetNode) {
        this.targetNode = targetNode;
    }

}
